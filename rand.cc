/*
	rand.cc		Random number generator
	Copyright (c) 2002, 2003, 2004, 2005 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "rand.h"
#include <stdlib.h>

bool	is_random = true;

// Random 0 .. num-1
int	get_random(int num)
{
	if (!is_random)
		return 0;

	int r = rand();
				// Cast num to double to avoid overflow
				// during multiplication
	int a = static_cast<int>(static_cast<double>(num) * r/(RAND_MAX+1.0));
	return a;
}

void	random_on()
{
	is_random = true;
}

void	random_off()
{
	is_random = false;
}
