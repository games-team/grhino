/*
	gutil.h		GRhino GNOME Frontend Utilities
	Copyright (c) 2005 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef GUTIL_H
#define GUTIL_H

#include <gtk/gtk.h>
#include <gnome.h>

#include "gtstream.h"

void error_message_box(const gtstream &bufstr);
GdkPixbuf *scale_pixbuf_from_xpm_data(const char *xpm[], int width, int height);

#endif /* GUTIL_H */
