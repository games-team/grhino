/*
	iter.cc		Iterators
	Copyright (c) 2000, 2001, 2003 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "iter.h"

// FIXME: Iterator data are shared and not thread-safe.

int	empty_endgame_size;
empty_endgame_entry *empty_endgame_head;
empty_endgame_entry empty_endgame_order[NUM_MOVE];

void init_endgame_iterator(byte_board_info *board, empty_endgame_info &info)
{
	int	count = 0;
	board_full_endgame_iterator	iter;
	iter.init_pos();

	info.empty_endgame_head = 0;
	do {
		if (board->is_empty(iter.pos)) {
			if (!count)
				info.empty_endgame_head = info.empty_endgame_order;
			else
				info.empty_endgame_order[count-1].next = &(info.empty_endgame_order[count]);

			info.empty_endgame_order[count].pos = iter.pos;
			info.empty_endgame_order[count].next = 0;
			count++;
		}
	} while (iter.next());
	info.empty_endgame_size = count;
}

