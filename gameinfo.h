/*
	gameinfo.h		Game States
	Copyright (c) 2004, 2005, 2006 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef GAMEINFO_H
#define GAMEINFO_H

#include "board.h"

// Initial board (num_history = 1, min_num_history = 1, max_num_history = 1)
//   board_history            .
//   move_history
//   player_history           B
//   time_history
//   clock_history_black  15:00.00
//   clock_history_white  15:00.00
// After first move F5 (num_history = 2, min_num_history = 1, max_num_history = 2)
//   board_history            .          .
//   move_history            F5
//   player_history           B          W
//   time_history           5.00
//   clock_history_black  15:00.00   14:55.00
//   clock_history_white  15:00.00   15:00.00
// After moves D6, C3 (num_history = 4, min_num_history = 1, max_num_history = 4)
//   board_history            .          .          .          .
//   move_history            F5         D6         C3
//   player_history           B          W          B          W
//   time_history           5.00       3.00       4.00
//   clock_history_black  15:00.00   14:55.00   14:55.00   14:51.00
//   clock_history_white  15:00.00   15:00.00   14:57.00   14:57.00

class game_info {
	public:
		enum game_result_type {	// When game_play = false
			game_result_end,
			game_result_timeout_black,
			game_result_timeout_white,
			game_result_resign_black,
			game_result_resign_white
		};

	private:
		bool	random_game;
		int	random_game_pieces;

		int	player;
		bool	game_play;
		game_result_type game_result;
		bool	use_clock;

		int	clock_history_black[NUM_MOVE+1];	// Unit 0.01 sec
	 	int	clock_history_white[NUM_MOVE+1];
	public:
	byte_board_info *board_ptr;

	// Opening information included
	bool		first_play_is_pass;		// For board
	byte_board_info	board_history[NUM_MOVE+1];
	int		player_history[NUM_MOVE+1];
	int		move_history[NUM_MOVE+1];
	int		time_history[NUM_MOVE+1];	// Unit 0.01 sec
	int		num_history;
	int		min_num_history;
	int		max_num_history;

	private:
		void new_game_remaining(int queue_size);
		void set_game_play_from_board();
	public:
		game_info();
		~game_info();

		void new_game_from_begin(int queue_size = 0);
		void new_game_from_board(const byte_board_info *b, int p);

		bool is_undoable() const;
		bool is_redoable() const;
		void undo();
		void redo();

		void place_piece(int pos, int time);
		void player_timeout();
		void player_resign();
		void game_end();

		bool is_random_game() { return random_game; }
		int  get_random_game_pieces() { return random_game_pieces; }
		int  get_player() { return player; }
		bool is_game_play() { return game_play; }
		game_result_type get_game_result() { return game_result; }
		bool is_use_clock() { return use_clock; }

		int  get_clock();
};

#endif /* GAMEINFO_H */
