/*
	gtp-rhino.h	GRhino GTP Frontend
	Copyright (c) 2005 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef GTP_H
#define GTP_H

#include <string>

class	id_error {};
class	command_error {};
class	syntax_error {};
class	command_quit {};
class	io_error {};

void	input_char(char &c);
void	output_char(char c);
void	output_flush();
void	input_line(std::string &str);
void	output_line(const std::string &str);
void	output_line(const char *str);
size_t	parse_id(const std::string &str, bool &has_id, unsigned &id);
size_t	skip_space(const std::string &str, size_t i);
size_t	skip_non_space(const std::string &str, size_t i);
size_t	read_unsigned(const std::string &str, size_t i, unsigned &num);
size_t	read_float(const std::string &str, size_t i, double &num);
size_t	read_color(const std::string &str, size_t i, int &color);
void	throw_command_error_if_end_of_line(const std::string &str, size_t i);
void	throw_syntax_error_if_end_of_line(const std::string &str, size_t i);
void	throw_if_extra_argument(const std::string &str, size_t i);
void	output_response(const std::string &str, bool has_id, unsigned id);
void	output_error(const std::string &str, bool has_id, unsigned id);
bool	process_unsigned_option(int argc, char *argv[], 
				const std::string &arg, int &i,
				const std::string &short_name,
				const std::string &long_name,
				unsigned min, unsigned max, unsigned &val);

#endif /* GTP_H */
