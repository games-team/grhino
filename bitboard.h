/*
	bitboard.h	Game Board Operations for BitBoard
	Copyright (c) 2004 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef BITBOARD_H
#define BITBOARD_H

#include "config.h"

#include "board.h"

struct bit_board_type {
	int	black[2];	// Row 1-4, 5-8
	int	white[2];	// Row 1-4, 5-8
};
extern bit_board_type bit_board_begin;

inline int pos_to_index_pos(int pos)
{
	return pos / 32;
}

inline int pos_to_bit_pos(int pos)
{
	return pos % 32;
}

inline int xy_to_index_pos(int x, int y)
{
	return y / 4;
}

inline int xy_to_bit_pos(int x, int y)
{
	return (y % 4) * 8 + x;
}

int count_bits(int x);
struct bit_board_info {
	private:
		char		move;
	public:
		bit_board_type	board;
		int		hash;
		bit_board_info();
		bit_board_info(const bit_board_type *board_init);

		bit_board_info& operator=(const bit_board_type *board_init);
		bit_board_info& operator=(const bit_board_info &src);

		bool is_empty(int x, int y) const {
			int mask = 0x80000000 >> xy_to_bit_pos(x, y);
			int index_pos = xy_to_index_pos(x, y);
			return ((board.black[index_pos]
				 | board.white[index_pos]) & mask) == 0;
		}

		bool is_empty(int pos) const {
			int mask = 0x80000000 >> pos_to_bit_pos(pos);
			int index_pos = pos_to_index_pos(pos);
			return ((board.black[index_pos]
				 | board.white[index_pos]) & mask) == 0;
		}

		int	board_black_score() const {
			return count_bits(board.black[0])+count_bits(board.black[1]);
		}
		int	board_white_score() const {
			return count_bits(board.white[0])+count_bits(board.white[1]);
		}
		int	board_diff_score() const {
			return board_black_score(board) - board_white_score(board);
		}
};

bool operator==(const bit_board_info &board1, const bit_board_info &board2);

#endif /* BITBOARD_H */
