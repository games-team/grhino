/*
	fdstream.h	C++ Stream From File Descripter
	Copyright (c) 2005 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net
	
	Adapt from The C++ Standard Library: A Tutorial and Reference

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __K_FDSTREAM_H
#define __K_FDSTREAM_H

#include "config.h"

#include <iostream>
#include <streambuf>
#include <stdio.h>

#if 0
class fdoutbuf : public std::streambuf {
		int fd;
	public:
		fdoutbuf(int _fd) : fd(_fd) {}
	protected:
		virtual int_type overflow(int_type c = std::char_traits<char>::eof()) {
			if (c != EOF) {
				char z = c;
				if (write(fd, &z, 1) != 1)
					return EOF;
			}
			return c;
		}
		virtual std::streamsize xsputn(const char *s, std::streamsize n) {
			return write(fd, s, n);
		}
};

class fdostream : private fdoutbuf, public std::ostream {
	public:
		fdostream(int fd) : fdoutbuf(fd), std::ostream(this) {}
};
#endif /* 0 */

#endif /* __K_FDSTREAM_H */
