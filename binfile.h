/*
	binfile.h	Binary File I/O
	Copyright (c) 2004 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef BINFILE_H
#define BINFILE_H

#include "config.h"
#include "board.h"

#include <string>

class binary_file_input {
	private:
		std::string	file;
		int		handle;
		bool		end_of_file;

		std::size_t	buffer_ptr;
		std::size_t	buffer_size;
		static const std::size_t max_buffer_size = 10000;
		unsigned char	buffer[max_buffer_size];

		void	fill_buffer();
		void	fill_buffer_if_required(std::size_t s);
		unsigned char	read_unsigned_char_nocheck() {
			return buffer[buffer_ptr++];
		}
	public:
		binary_file_input(const std::string &f);
		~binary_file_input();

		unsigned char	read_unsigned_char();
		int		read_int();
		unsigned	read_unsigned_compress();
		int		read_int_compress() {
				return static_cast<int>(read_unsigned_compress());
		}
		void		read_board(byte_board_info& b);
};

class binary_file_output {
	private:
		std::string	file;
		int		handle;

		std::size_t	buffer_size;
		static const std::size_t max_buffer_size = 10000;
		unsigned char	buffer[max_buffer_size];

		void	flush_buffer();
		void	flush_buffer_if_required(std::size_t s);
		void	write_unsigned_char_nocheck(unsigned char c) {
			buffer[buffer_size++] = c;
		}
	public:
		binary_file_output(const std::string &f);
		~binary_file_output();

		void	write_unsigned_char(unsigned char c);
		void	write_int(int i);
		void	write_unsigned_compress(unsigned i);
		void	write_int_compress(unsigned i) {
			write_unsigned_compress(static_cast<unsigned>(i));
		}
		void	write_board(const byte_board_info& b);
};

#endif /* BINFILE_H */
