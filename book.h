/*
	book.h		Open book database
	Copyright (c) 2001, 2002, 2004 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef BOOK_H
#define BOOK_H

#include "board.h"

typedef int *symmetry_type;

int	read_int(int handle);
void	write_int(int handle, int i);
int	read_char(int handle);
void	write_char(int handle, int i);
unsigned	read_unsigned_compress(int handle);
void	write_unsigned_compress(int handle, unsigned i);

symmetry_type	get_symmetry(int first_move);

const char *get_book_file();
const char *get_book_data_file();

void	set_book_random(size_t r);
size_t	get_book_random();
void	book_init();
int	book_move(byte_board_info *board, int move_history[NUM_MOVE+1], int player_history[NUM_MOVE+1]);

#endif /* BOOK_H */
