/*
	opening.cc	Opening database
	Copyright (c) 2001, 2004 Kriang Lerdsuwanakij
	email:		lerdsuwa@users.sourceforge.net

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <string.h>

#include "opening.h"

#define N_(x) x
#include "opening-dat.h"

int get_num_opening()
{
	return NUM_OPENING_DATA;
}

const char *get_opening_name(int id)
{
	return opening_data[id].name;
}

int get_opening_id(const char *s)
{
	for (int i = 0; i < get_num_opening(); ++i) {
		if (!strcmp(s, opening_data[i].name))
			return i;
	}
	return 0;
}

int get_opening_player(int id)
{
	return opening_data[id].next_player;
}

int get_opening_player(const char *s)
{
	return get_opening_player(get_opening_id(s));
}

int get_opening_num_move(int id)
{
	return opening_data[id].num_move_sequence;
}

int get_opening_num_move(const char *s)
{
	return get_opening_num_move(get_opening_id(s));
}

const byte_board_type *get_opening_board(int id)
{
	return &opening_data[id].board;
}

const byte_board_type *get_opening_board(const char *s)
{
	return get_opening_board(get_opening_id(s));
}

void get_opening_move_sequence(int id, std::deque<int> &d)
{
	if (d.size())
		d.erase(d.begin(), d.end());
	for (size_t i = 0; i < opening_data[id].num_move_sequence; ++i)
		d.push_back(static_cast<int>(
			opening_data[id].move_sequence[i]));
}

void get_opening_move_sequence(const char *s, std::deque<int> &d)
{
	get_opening_move_sequence(get_opening_id(s), d);
}
